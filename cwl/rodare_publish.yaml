cwlVersion: v1.0
class: CommandLineTool
baseCommand: python3

label: Create Publication
doc: This tool allows to create a RODARE publication including metadata from HELIPORT
  and uploading data. The tool uses the rodare_publish.py script for RODARE interaction.

inputs:
  rodare_publish_script:
    type: File
    inputBinding:
      position: 0
    label: Thes needs to be the rodare_publish.py script.

  help:
    type: boolean
    inputBinding:
      position: 1
      prefix: --help
    label: Show a help message and exit

  files:
    type: File[]
    inputBinding:
      position: 2
    label: Files to be published

  zipped:
    type: Directory[]
    inputBinding:
      position: 3
      prefix: --zipped
    label: Directories to be published as zip file

  title:
    type: string?
    inputBinding:
      position: 4
      prefix: --title
    label: Title of the publication

  description:
    type: string?
    inputBinding:
      position: 5
      prefix: --description
    label: Description of the publication

  name:
    type: string?
    inputBinding:
      position: 6
      prefix: --name
    label: Name of the first author

  orcid:
    type: string?
    inputBinding:
      position: 7
      prefix: --orcid
    label: ORCID of the first author

  affiliation:
    type: string?
    inputBinding:
      position: 8
      prefix: --affiliation
    label: Affiliation of the first author e.g. "HZDR"

  upload_type:
    type: string?
    inputBinding:
      position: 9
      prefix: --upload_type
    label: 'Kind of data to be published. Available options are: dataset, image, video,
      software, other. Default is dataset. Specifying only the first letter of upload_type
      is sufficient.'

  closed_access:
    type: boolean
    inputBinding:
      position: 10
      prefix: --closed_access
    label: Make publication not visible to the public

  license:
    type: string?
    inputBinding:
      position: 11
      prefix: --license
    label: License under which the data is available. For closed_access publications
      license is ignored. The default and recommended value is CC-BY-4.0. (Creative
      Commons License)

  wait:
    type: boolean
    inputBinding:
      position: 12
      prefix: --wait
    label: Wait until all uploads have completed

  quiet:
    type: boolean
    inputBinding:
      position: 13
      prefix: --quiet
    label: Never ask for user input

  not_a_test:
    type: boolean
    inputBinding:
      position: 14
      prefix: --not_a_test
    label: Create a publication in RODARE (instead of rodare-test)

  zip_output_dir:
    type: string?
    inputBinding:
      position: 15
      prefix: --zip_output_dir
    label: Directory to store zip files in. (needs to be on /bigdata)

  project:
    type: string?
    inputBinding:
      position: 16
      prefix: --project
    label: Name, handle, link or ID of HELIPORT project (for metadata)

  gate_proposal:
    type: string?
    inputBinding:
      position: 17
      prefix: --gate_proposal
    label: ID or URL of a gate proposal (for metadata)

  existing_publication:
    type: string?
    inputBinding:
      position: 18
      prefix: --existing_publication
    label: URL or ID of an existing RODARE publication. If not supplied a new publication
      is created.

  settings:
    type: File?
    inputBinding:
      position: 19
      prefix: --settings
    label: JSON file with publication settings. Settings that are not provided here
      are looked up in this file. If the specified file does not exist it is created
      as a template using the current settings.

  heliport_token:
    type: HELIPORTToken
    inputBinding:
      position: 20
      prefix: --heliport_token
    label: API Token for HELIPORT. You can get a token under user > settings.

  rodare_token:
    type: string?
    inputBinding:
      position: 21
      prefix: --rodare_token
    label: API Token for RODARE. It is not recomended to specify the token here as
      this parameter is visible to everyone with access rights. Instead add it in
      HELIPORT under user > settings > Manage Logins. RODARE token can be generated
      at https://rodare.hzdr.de/account/settings/applications/ and https://rodare-test.hzdr.de/account/settings/applications/.

outputs: {}
