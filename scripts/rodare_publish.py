#!/usr/bin/env python3
import pathlib
import argparse
import json
import socket
import shutil
import mimetypes
import re
from time import sleep
from datetime import datetime
from collections import namedtuple, Counter
from itertools import chain
from pprint import pprint
try:
    from functools import cached_property
except ImportError:
    cached_property = property
    print('ERROR: Some things might not work because of your old python version. Please use 3.8 or higher.')


class ShortMetavarHelpTextFormatter(argparse.HelpFormatter):
    def __init__(self, *args, **kwargs):
        kwargs['max_help_position'] = 50
        super().__init__(*args, **kwargs)

    def _format_action_invocation(self, action):
        metavar = action.metavar
        action.metavar = 'X'
        result = super()._format_action_invocation(action)
        action.metavar = metavar
        return result

    def _fill_text(self, text, width, indent):
        if text.startswith('R|'):
            return text[2:].format(prog=self._prog)
        return super()._fill_text(text, width, indent)


arg_parser = argparse.ArgumentParser(formatter_class=ShortMetavarHelpTextFormatter, description='''
Tool for creating RODARE publications and adding metadata as well as uploading data. 
Integration with HELIPORT is also supported.
''', epilog='''R|
--- EXAMPLES ---
Metadata from heliport project 16:
{prog} --project 16

Metadata from gate proposal:
{prog} --gate_proposal 21100042-ST

Upload csv files from two directories:
{prog} --title "some csv publication" run1/*.csv run2/*.csv

Upload directory as zip:
{prog} --title "some zip publication" --zipped file_directory --zip_output_dir /bigdata/mydir/zipfiles
''')

upload_types = ['dataset', 'image', 'video', 'software', 'other']
abbreviations = {
    'HZDR': 'Helmholtz-Zentrum Dresden-Rossendorf'
}

heliport_url = 'https://heliport.hzdr.de/app'
hzdr_prefix = '20.500.12865'
handle_proxy = 'hdl.handle.net'

CreatorMetadata = namedtuple('CreatorMetadata', 'name orcid affiliation')
PublicationMetadata = namedtuple(
    'PublicationMetadata',
    'title description creators upload_type access_right license related_identifiers')


def translate(abbreviation):
    return abbreviations.get(abbreviation, abbreviation)


def serialize(data):
    """Recursively converts a namedtuple to dict ignoring None values"""
    if hasattr(data, '_asdict'):
        items = data._asdict().items()
        return {k: serialize(v) for k, v in items if v is not None}

    elif isinstance(data, list):
        return [serialize(v) for v in data]

    else:
        return data


def remove_prefix(string, prefix):
    if string.startswith(prefix):
        return string[len(prefix):]
    else:
        return string


def compatible(a, b):
    return a is None or b is None or a == b


def matching(a, b):
    return a is not None and b == a


def same_creator(c, name, orcid, affiliation):
    cond = compatible(c.orcid, orcid) and compatible(c.name, name) and compatible(c.affiliation, affiliation)
    return cond and (matching(c.orcid, orcid) or matching(c.name, name))


def guess_file_type(file_path):
    t, c = mimetypes.guess_type(file_path)
    t = str(t)
    if t.startswith('video/') or t.startswith('audio/'):
        return 'video'
    if t.startswith('image/'):
        return 'image'
    if file_path.suffix.lower() in {'.c', '.cgi', '.pl', '.class', '.cpp', '.cs', '.h', '.java', '.php', '.py',
                                    '.sh', '.swift', '.vb', '.js', '.html', 'c++', '.hpp', '.go', '.r', '.swift',
                                    '.dart', '.kt', '.m', '.rb', '.rs', '.f90', '.for', '.f', '.ipynb', '.rmd',
                                    '.md', '.mat', '.exe', '.json', '.ova', '.nb', '.pth', '.jl', '.bam', '.whl',
                                    '.yaml', '.rds', '.npy', '.stl', '.mx', '.do', '.ino', '.v', '.sl', '.make',
                                    '.docker', '.pandas'}:
        return 'software'
    return None


def glob(pattern):
    if pathlib.Path(pattern).is_absolute():
        return pathlib.Path('/').glob(pattern[1:])
    else:
        return pathlib.Path().glob(pattern)


def get_input(message):
    try:
        return input(message)
    except EOFError:
        raise EOFError(f'ERROR when asking for user input "{message}". Use --quiet to disable questions.')


def parse_int(input_str):
    try:
        return int(input_str)
    except (ValueError, TypeError):
        return None


class PublicationManager:
    def __init__(self, command_line_args):
        self.command_line_args = command_line_args
        self.settings = self.command_line_args
        self.printed_help_message = False

    def read_settings(self):
        settings_path = self.command_line_args.settings or 'publication_settings.json'
        try:
            with open(settings_path) as s:
                return json.loads(s.read())
        except FileNotFoundError:
            return None
        except json.JSONDecodeError as e:
            print(f'ERROR: Settings file contains invalid JSON: {settings_path}')
            print(e)
            exit(1)

    def write_settings(self, **kwargs):
        settings_path = self.command_line_args.settings or 'publication_settings.json'
        settings_dict = self.read_settings()
        if settings_dict is None:
            settings_dict = dict(self.command_line_args._get_kwargs())
        settings_dict.update(kwargs)

        with open(settings_path, 'w') as s:
            s.write(json.dumps(settings_dict, indent=3))
        print('successfully created template settings file', settings_path)
        print(f"Please REVIEW this newly generated settings file.")

    def parse_settings(self):
        settings_path = self.command_line_args.settings
        if settings_path is None:
            settings_dict = self.read_settings()
            if settings_dict is not None:
                self.settings = argparse.Namespace(**settings_dict)
            else:
                self.settings = argparse.Namespace()
        else:
            settings_dict = self.read_settings()
            if settings_dict is not None:
                self.settings = argparse.Namespace(**settings_dict)
            else:
                self.write_settings()
                exit(0)

        for key, value in self.command_line_args._get_kwargs():
            if value is not None or not hasattr(self.settings, key):
                setattr(self.settings, key, value)

    def print_error_help(self, message, arg=None):
        print('ERROR:', message)
        if arg is not None:
            print('specify e.g. with:', arg)
        if not self.printed_help_message:
            print('for more information use: -h')
        self.printed_help_message = True

    def heliport_api_result_list(self, url, **kwargs):
        next_url = url
        results = []
        first_time = True
        while next_url is not None:
            if first_time:
                first_time = False
                response = self.heliport_request(next_url, **kwargs)
            else:
                response = self.heliport_request(full_url=next_url)
            if response is None:
                return None
            if 'results' not in response:
                print('ERROR got invalid response from HELIPORT:')
                pprint(response)
                return None
            results.extend(response['results'])
            next_url = response.get('next')
        return results

    @cached_property
    def heliport_token(self):
        token = self.settings.heliport_token
        if token is None:
            self.print_error_help('heliport token not specified', '-ht 1234')
            if not self.settings.quiet:
                answer = get_input('Enter one Now? [y/N] ')
                if answer.lower().startswith('y'):
                    print(f"Get a token from {heliport_url}/user/token/list")
                    token = get_input('Token: ')
                    if len(token) < 10:
                        print('ERROR: could not get token')
                    else:
                        self.write_settings(heliport_token=token)
                    return token
            exit(1)
        prefixes = ['Authorization: ', 'Token ', 'Bearer']
        for p in prefixes:
            if token.startswith(p):
                token = token[len(p):]
        return token

    @cached_property
    def rodare_token(self):
        def normalize(t):
            if t is None:
                return None
            return remove_prefix(t, 'access_token')

        result = normalize(self.settings.rodare_token)
        if result is None:
            available_tokens = self.heliport_api_result_list('/api/tokens') or []
            available_tokens.sort(key=lambda t: 'rodare' not in t['name'].lower())
            for login_info in available_tokens:
                token = normalize(login_info['token'])
                try:
                    self.rodare_request('/deposit/depositions', explicit_token=token, raise_not_ok=True)
                    print('using rodare token from HELIPORT:', login_info['name'])
                    result = token
                except requests.HTTPError:
                    pass

        if result is None:
            self.print_error_help('rodare token not specified and not found in HELIPORT', '-rt 1234')
            print(f'URL to get a token ("deposit:actions" required): {self.rodare_url}/account/settings/applications/')
            print(f'URL to add the token: {heliport_url}/user/login/list/')
            exit(1)
        return result

    @cached_property
    def rodare_url(self):
        return 'https://rodare.hzdr.de' if self.settings.not_a_test else 'https://rodare-test.hzdr.de'

    @staticmethod
    def handle_response(response, raise_not_ok):
        if raise_not_ok:
            response.raise_for_status()
        try:
            result = response.json()
        except (json.JSONDecodeError, ValueError) as e:  # ValueError in old versions of requests module
            print(f'ERROR: {response.url} returned invalid json (status {response.status_code})')
            pprint(response.content)
            print(e)
            return None

        if not response.ok:
            print(f'ERROR at {response.url}:')
            pprint(result)
            return None
        return result

    @staticmethod
    def handle_response_error(error, url):
        print(f'ERROR: could not reach {url}')
        print(error)
        print('Do you need access to HZDR internal network?')

    def heliport_request(self, url=None, method=None, raise_not_ok=False, full_url=None, no_token=False,
                         kwargs_as_json=False, additional_kwargs=None, **kwargs):
        request_url = None
        if url is not None:
            request_url = f'{heliport_url}{url}'
        if full_url is not None:
            request_url = full_url
        assert request_url is not None, 'neither url nor full_url specified'
        if method is None:
            method = requests.get

        headers = {'Content-Type': 'application/json'}
        if not no_token:
            headers['Authorization'] = f'Token {self.heliport_token}'
        if additional_kwargs:
            kwargs.update(additional_kwargs)
        try:
            if kwargs_as_json:
                response = method(request_url, json=kwargs, headers=headers)
            else:
                response = method(request_url, params=kwargs, headers=headers)

        except requests.ConnectionError as e:
            self.handle_response_error(e, request_url)
            return None
        return self.handle_response(response, raise_not_ok)

    def rodare_request(self, relative_url=None, method=None, data=None, full_url=None,
                       raise_not_ok=False, explicit_token=None, **kwargs):
        if method is None:
            method = requests.get
        if explicit_token is None:
            kwargs['access_token'] = self.rodare_token
        else:
            kwargs['access_token'] = explicit_token
        request_url = None
        if relative_url is not None:
            request_url = f'{self.rodare_url}/api{relative_url}'
        if full_url is not None:
            request_url = full_url
        assert request_url is not None, 'neither url nor full_url specified'

        try:
            response = method(request_url, json=data, params=kwargs,
                              headers={'Content-Type': 'application/json'})
        except requests.ConnectionError as e:
            self.handle_response_error(e, request_url)
            return None
        return self.handle_response(response, raise_not_ok)

    @cached_property
    def existing_project(self):
        project = self.settings.project
        if project is None:
            return None

        project_id = None
        if str(project).isnumeric():
            project_id = project

        if project_id is None:
            match = re.match('.*/projects?/([0-9]+)', project)
            if match is not None:
                project_id = match.group(1)

        handle = project
        handle = remove_prefix(handle, 'https://')
        handle = remove_prefix(handle, 'http://')
        handle = remove_prefix(handle, f'{handle_proxy}/')
        handle = remove_prefix(handle, 'doi.org/')
        handle = remove_prefix(handle, f'{hzdr_prefix}/')
        if project_id is None:
            for possible_handle_value in [project, f'{hzdr_prefix}/{handle}', handle]:
                projects = self.heliport_api_result_list('/api/projects', handle=possible_handle_value)
                if len(projects) == 1:
                    project_id = projects[0]['project_id']
                    break

        if project_id is None:
            projects = self.heliport_api_result_list('/api/projects', search=project)
            if len(projects) == 1:
                project_id = projects[0]['project_id']
            else:
                if len(projects) == 0:
                    print(f'ERROR: no project found matching (tried id, handle, link and search term): {project}')
                    candidates = self.heliport_api_result_list('/api/projects', search=project[:3])
                    if len(candidates) == 0:
                        candidates = self.heliport_api_result_list('/api/projects')
                    if len(candidates) == 0:
                        print('ERROR: you have no HELIPORT Project')
                else:
                    print(f'ERROR: "{project}" did not uniquely identify a HELIPORT project.')
                    candidates = projects
                if len(candidates) > 0:
                    print('Candidates are:')
                    for p in candidates:
                        print(f'{p["project_id"]:>5}: {p["label"]}')
                exit(1)

        response = self.heliport_request(f'/api/projects/{project_id}')
        if response is None:
            self.print_error_help('project could not be found')
        else:
            print('Using HELIPORT project:', response['label'])
        return response

    @cached_property
    def existing_publication(self):
        rodare_id = self.settings.existing_publication

        if rodare_id is not None:
            if not rodare_id.isnumeric():
                possible_ids = re.findall('/([0-9]+)/', f'{rodare_id}/')
                if len(possible_ids) == 1:
                    rodare_id = possible_ids[0]
                else:
                    print('ERROR: not a valid publication URL or publication ID:', rodare_id)
                    exit(1)
            result = self.rodare_request(f'/deposit/depositions/{rodare_id}')
        else:
            result = None
        return result

    @cached_property
    def gate_proposal(self):
        gate_proposal = self.settings.gate_proposal
        if gate_proposal is None:
            return None

        url_match = re.match('.*pnr=([0-9]+)', gate_proposal)
        if url_match is not None:
            gate_proposal = url_match.group(1)

        if gate_proposal.isnumeric():
            candidates = self.heliport_api_result_list('/api/gate/projects/', gate_id=gate_proposal)
        else:
            candidates = self.heliport_api_result_list('/api/gate/projects/', proposal=gate_proposal)

        if candidates:
            return candidates[0]
        else:
            print('ERROR: No gate proposal found matching', gate_proposal)
            print('Proposals are only synced about once a day.')
            exit(1)

    def ask_or_take(self, key, value):
        if not value or len(str(value)) < 3:
            if value is None:
                value = ''
            if self.settings.quiet:
                print(f'ERROR: {key} "{value}" too short')
                exit(1)
            else:
                while len(value) < 3:
                    value = get_input(f'{key} "{value}" too short. Enter new {key.lower()}: ')
        return value

    @cached_property
    def title(self):
        result = self.settings.title
        if result is None and self.existing_publication:
            result = self.existing_publication.get('title')
        if not result and self.existing_project:
            result = self.existing_project.get('label')
        if not result and self.gate_proposal:
            result = self.gate_proposal.get('label')
        return self.ask_or_take('Title', result)

    @cached_property
    def description(self):
        result = self.settings.description
        if result is None and self.existing_publication:
            metadata = self.existing_publication.get('metadata')
            if metadata:
                result = metadata.get('description')
        if not result and self.existing_project:
            result = self.existing_project.get('description')
        if not result and self.gate_proposal:
            result = self.gate_proposal.get('description')
        return self.ask_or_take('Description', result)

    @cached_property
    def project_members(self):
        members = set()
        if self.existing_project:
            co_owners = self.existing_project.get('co_owners') or []
            members = {self.existing_project.get('owner'), *co_owners}
            members.discard(None)
        if not members and self.gate_proposal:
            co_owners = self.gate_proposal.get('co_owners') or []
            experimentalists = self.gate_proposal.get('experimentalists') or []
            members = {self.gate_proposal.get('owner'), *co_owners, *experimentalists}
            members.discard(None)

        result = []

        def insert(cname, corcid, caffiliation):
            for other in result:
                if same_creator(other, cname, corcid, caffiliation):
                    result.remove(other)
                    corcid = other.orcid or corcid
                    cname = other.name or cname
                    caffiliation = other.affiliation or caffiliation
                    break

            result.append(CreatorMetadata(cname, corcid, caffiliation))

        for m in members:
            member = self.heliport_request(f'/api/users/{m}')
            name = member.get('display_name')
            orcid = member.get('orcid')
            affiliation = member.get('affiliation')
            if name is not None:
                r = re.match(r'([^()]+) \((.*)\)', name)
                if r is not None:
                    name = r.group(1)
                    if affiliation is None and r.group(2).lower() != 'extern':
                        affiliation = translate('HZDR')
            if name is not None and ', ' not in name:
                words = name.split(' ')
                if len(words) > 1:
                    name = f'{words[-1]}, {" ".join(words[:-1])}'
                    print(f'Corrected name format to "{name}"')
            insert(name, orcid, affiliation)

        if self.existing_publication:
            metadata = self.existing_publication.get('metadata', dict())
            creators = metadata.get('creators', [])
            for creator in creators:
                insert(creator.get('name'), creator.get('orcid'), creator.get('affiliation'))
        return result

    @cached_property
    def first_creator(self):
        name = self.settings.name
        orcid = self.settings.orcid
        affiliation = translate(self.settings.affiliation)
        return CreatorMetadata(name, orcid, affiliation)

    @cached_property
    def creators(self):
        f = self.first_creator
        members = self.project_members
        result = []
        if f.name is None and f.orcid is None and f.affiliation is None:
            result = members
        else:
            for m in members:
                if same_creator(m, f.name, f.orcid, f.affiliation):
                    f = CreatorMetadata(f.name or m.name, f.orcid or m.orcid, f.affiliation or m.affiliation)
                    members.remove(m)
                    break
            result = [f, *members]
        if len(result) == 0 and not self.settings.quiet:
            name = get_input('No creator specified. Please enter "Family name, given names": ')
            result = [CreatorMetadata(name, None, None)]
        for c in result:
            if c.name is None:
                print(f'Removing creator without name: orcid={c.orcid} affiliation={c.affiliation}')
        result = [c for c in result if c.name is not None]
        return result

    @cached_property
    def upload_type(self):
        types = Counter([guess_file_type(p) for p in self.file_paths])
        result = self.settings.upload_type

        if result is None:
            if len(types) == 1:
                result = list(types.keys())[0]
            elif len(types) > 1:
                ((mc_type, mc_count), (sc_type, sc_count)) = types.most_common(2)
                if mc_count > 4 * sc_count:
                    result = mc_type
            if result is not None:
                print('Detected upload_type:', result)

        if result is None:
            result = 'dataset'
        if len(result) == 1:
            for potential_type in upload_types:
                if potential_type.startswith(result):
                    result = potential_type
        return result

    @cached_property
    def access_right(self):
        result = self.settings.access_right
        if result is None:
            result = 'open'
        return result

    @cached_property
    def publication_license(self):
        result = self.settings.license
        if result is None:
            result = 'CC-BY-4.0'
        return result

    @cached_property
    def publication(self):
        result = self.existing_publication
        if result is None:
            result = self.rodare_request('/deposit/depositions', requests.post, data={})
        if result is None or result.get('id') is None or result.get('links') is None:
            print('ERROR: Could not create publication')
            print(result)
            exit(1)

        if self.existing_project is not None and 'links' in result and result['links'].get("html"):
            publications = self.heliport_api_result_list("/api/publications/")
            project_id = parse_int(self.existing_project.get("project_id"))
            publication_url = result['links']['html']
            if not any(
                project_id in publication.get("projects", [])
                and publication_url == publication.get("url")
                for publication in publications
            ):
                new_publication = self.heliport_request(
                    "/api/publications/", requests.post,
                    kwargs_as_json=True,
                    additional_kwargs=dict(
                        description=f"Automatic data publication to RODARE ({datetime.now().strftime('%d %b %Y')})",
                        projects=[project_id],
                        url=publication_url,
                    )
                )
                print("Added new publication to heliport project")

        return result

    @cached_property
    def publication_id(self):
        return self.publication['id']

    @cached_property
    def related_identifiers(self):
        result = []
        if self.existing_project:
            project_identifier = self.existing_project.get("persistent_id")
            if project_identifier and project_identifier != "None":
                result.append({
                    "identifier": project_identifier,
                    "relation": "isSupplementedBy",
                    "scheme": "handle",
                })
        return result

    @cached_property
    def metadata(self):
        return PublicationMetadata(self.title, self.description, self.creators, self.upload_type,
                                   self.access_right, self.publication_license, self.related_identifiers)

    @cached_property
    def file_paths(self):
        results = []
        for file in chain(*(glob(f) for f in self.settings.files if hzdr_prefix not in f)):
            results.append(file.resolve())
        results.extend(self.ssh_data_source_files)
        results.extend(self.zip_files)
        return results

    @cached_property
    def zip_files(self):
        zip_destination = pathlib.Path()
        if self.settings.zip_output_dir is not None:
            zip_destination = pathlib.Path(self.settings.zip_output_dir)

        result = []
        for directory in self.settings.zipped or []:
            source = pathlib.Path(directory)
            destination = zip_destination / f'{source.name}'
            print("compressing", source, "to", str(destination) + ".zip")
            new_file = shutil.make_archive(str(destination), "zip", base_dir=source)
            result.append(pathlib.Path(new_file))

        return result

    @cached_property
    def data_source_handles(self):
        results = []
        for file_description in self.settings.files:
            prefixes = [f'http://{handle_proxy}/{hzdr_prefix}/', f'https://{handle_proxy}/{hzdr_prefix}/',
                        f'{handle_proxy}/{hzdr_prefix}/', f'{hzdr_prefix}/']
            for prefix in prefixes:
                if file_description.startswith(prefix):
                    results.append(file_description[len(prefix):])
                    break
            else:
                if f'{handle_proxy}/' in file_description:
                    print(f'ERROR: {file_description} is not a valid datasource')

        return results

    @cached_property
    def data_sources(self):
        results = []
        for handle in self.data_source_handles:
            for handle_variant in [
                f'{hzdr_prefix}/{handle}',
                f'{handle_proxy}/{hzdr_prefix}/{handle}',
                f'http://{handle_proxy}/{hzdr_prefix}/{handle}',
                f'https://{handle_proxy}/{hzdr_prefix}/{handle}'
            ]:
                data_source_parameters = self.heliport_api_result_list('/api/data-sources/',
                                                                       persistent_id=handle_variant)
                if data_source_parameters:
                    results.append(data_source_parameters[0])
                    break
            else:
                print(f'ERROR: data source "{handle}" was not found')
        return results

    @cached_property
    def ssh_data_source_files(self):
        results = []
        for data_source in self.data_sources:
            label = data_source.get('label')
            uri = data_source.get('uri')
            ssh_prefix = 'ssh://'
            path = pathlib.Path(remove_prefix(uri, ssh_prefix))
            if uri.startswith(ssh_prefix) and path.exists():
                results.append(path)
        return results

    @cached_property
    def upload_url(self):
        try:
            return self.publication['links']['uploadviaurl']
        except KeyError:
            print(f'ERROR: publication has no "uploadviaurl"')
            return None

    @cached_property
    def html_url(self):
        try:
            return self.publication['links']['html']
        except KeyError:
            print(f'ERROR: publication has no "html"')
            return None

    @cached_property
    def system_upload_url(self):
        url = self.upload_url
        if url is None:
            return None
        if socket.gethostname().endswith('.cluster'):
            print('Detected source location: fes')
            return f'{url}/fes'
        elif socket.gethostname() == 'uts':
            print('Detected source location: uts')
            return f'{url}/uts'
        return None

    def post_publication_metadata(self):
        m = serialize(self.metadata)
        if m['upload_type']:
            m['image_type'] = 'other'
        data = dict(metadata=m)

        print('Sending publication Metadata')
        # pprint(m)
        self.rodare_request(f'/deposit/depositions/{self.publication_id}', requests.put, data)

    def upload_files(self):
        if not self.file_paths:
            if self.settings.files:
                print('No matching files found', ', '.join(self.settings.files))
            return

        url = self.system_upload_url
        if url is None:
            print(f'ERROR: uploading from {socket.gethostname()} is not supported by RODARE')
            for f in self.file_paths:
                print(f)
            return

        for i, file in enumerate(self.file_paths):
            print('starting upload:', file)
            if url.endswith('/fes') and \
                    pathlib.Path('/bigdata') not in file.parents and \
                    pathlib.Path('/net') not in file.parents:
                print(f'WARNING: file is probably not accessible by rodare via fes. (neither /bigdata nor /net)')

            self.rodare_request(full_url=url, method=requests.post, path=file)
            if i % 48 == 0 and i > 0:
                print(f'Pausing one minute to not accede servers request limit. ({i}/{len(self.file_paths)}: {file})',
                      flush=True)
                sleep(60)

    def query_upload_status(self):
        return self.rodare_request(full_url=self.upload_url)

    def wait_for_uploads(self):
        values = []
        all_finished = len(self.file_paths) == 0
        while not all_finished:
            new_status = list(self.query_upload_status().values())
            all_finished = True
            for status in new_status:
                if status.get('state') == 'PENDING':
                    all_finished = False
                if status not in values:
                    values.append(status)
                    print(
                        datetime.now().strftime("%H:%M:%S"),
                        status.get('state'),
                        status.get("location", "https"),
                        flush=True
                    )
            if not all_finished:
                sleep(2)

    def publish(self):
        self.parse_settings()
        self.post_publication_metadata()
        self.upload_files()
        print('FINISHED you can find the publication here:', self.html_url)
        if self.settings.wait:
            self.wait_for_uploads()
            print('FINISHED UPLOADING you can find the publication here:', self.html_url)

        if not self.settings.not_a_test:
            print('If you want to create a publication in RODARE (not rodare-test) use the setting --not_a_test')


def main():
    manager = PublicationManager(arg_parser.parse_args())
    manager.publish()


arg_parser.add_argument('files', type=str, help='Files to be published. Including support for "*" in path.', nargs='*')
arg_parser.add_argument('-z', '--zipped', type=str, help='Directories to be published as zip file.', nargs='*')
arg_parser.add_argument('-t', '--title', type=str, help='Title of the publication')
arg_parser.add_argument('-d', '--description', type=str, help='Description of the publication')
arg_parser.add_argument('-n', '--name', type=str, help='Name of the first author')
arg_parser.add_argument('-o', '--orcid', type=str, help='ORCID of the first author')
arg_parser.add_argument('-a', '--affiliation', type=str,
                        help='Affiliation of the first author e.g. "HZDR"')

arg_parser.add_argument('-u', '--upload_type',
                        choices=upload_types + [u[0] for u in upload_types],
                        help=f'''
                        Kind of data to be published. Available options are: {', '.join(upload_types)}.
                        Default is dataset. Specifying only the first letter of upload_type is sufficient.
                        ''')
arg_parser.add_argument('-c', '--closed_access',
                        dest='access_right', action='store_const', const='closed',
                        help='Make publication not visible to the public')
arg_parser.add_argument('-l', '--license', type=str,
                        help='''
                        License under which the data is available. For closed_access publications license is ignored.
                        The default and recommended value is CC-BY-4.0. (Creative Commons License)
                        ''')

arg_parser.add_argument('-w', '--wait', action='store_true', default=None,
                        help='''Wait until all uploads have completed''')
arg_parser.add_argument('-q', '--quiet', action='store_true', default=None,
                        help='''Never ask for user input''')
arg_parser.add_argument('--not_a_test', action='store_true', default=False,
                        help='''Create a publication in RODARE (instead of rodare-test)''')
arg_parser.add_argument('--zip_output_dir', type=str, default=None,
                        help='''Directory to store zip files in. Needs to be accessible by this program and RODARE.''')

arg_parser.add_argument('-p', '--project', type=str,
                        help='Name, handle, link or ID of HELIPORT project (for metadata)')
arg_parser.add_argument('-g', '--gate_proposal', type=str,
                        help='ID or URL of a gate proposal (for metadata)')
arg_parser.add_argument('-e', '--existing_publication', type=str,
                        help='''
                        URL or ID of an existing RODARE publication. If not supplied a new publication is created.
                        ''')
arg_parser.add_argument('-s', '--settings', type=str,
                        help='''
                        Path of a json file with publication settings. Settings that are not provided as command line
                        arguments are looked up in this file. If the file "publication_settings.json" exists it is used
                        by default. If a specified file does not exist it is created as a template using the current 
                        settings.
                        ''')

arg_parser.add_argument('-ht', '--heliport_token', type=str,
                        help=f'''
                        API Token for HELIPORT. You can get a token here: {heliport_url}/user/settings/
                        ''')
arg_parser.add_argument('-rt', '--rodare_token', type=str,
                        help=f'''
                        API Token for RODARE. RODARE token is automatically used from HELIPORT if available. 
                        You can get a token here: https://rodare-test.hzdr.de/account/settings/applications/ 
                        bzw https://rodare.hzdr.de/account/settings/applications/
                        ''')

if __name__ == '__main__':
    try:
        import requests
    except ModuleNotFoundError:
        print('The module "requests" is required to run this tool please install it:\n  pip install requests')
        exit(1)
    main()
